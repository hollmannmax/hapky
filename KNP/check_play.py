#! /usr/bin/env python3
# -*- coding: utf-8 -*-

import getopt
import sys
import time
import os
import traceback

# importing local libs
sys.path.append('..'+os.sep+"share")
import game
import fileutils

num_iterations = 100

CZ = True # "ENG" is understood as default

def make_check():
# DOES THE player MODULE EXIST AT ALL?

result=True
# unless all the checkes are passed, this will be set to False.

try:
import player
message= 'OK: Module player.py exists and can be imported.\n'
except:
message= 'Error: Module player.py does not exist or is syntactically incorrect!\n'
result=False
# finish then, there is no way how to proceed
return result,message

# does the doc string exist and does it have less than 80 chars?
# try:
# docstring = player.MyPlayer.__doc__
# if docstring is None:
# message = message+"Error: docstring does not exist \n"
# result=False
# else:
# if len(docstring)==0:
# message = message+"Error: docstring is empty \n"
# result=False
# elif len(docstring)>80:
# message = message+"Error: docstring is too long, it has %d characters \n"%len(docstring)
# result=False
# else:
# message = message+"OK: docstring is: "+docstring+"\n"
# except:
# pass

# CAN THE MyPlayer INSTANCE BE CREATED?
try:
my_player = player.MyPlayer()
if CZ:
message += "OK: instance/objekt hráče MyPlayer v pořádku vytvořena."
else:
message += "OK: MyPlayer instance created."
except:
result = False
if CZ:
message += "Chyba: instance hráče MyPlayer nemohla být vytvořena."
else:
message += "Error: MyPlayer instance cannot be created!"
message += "\n"

# # CAN THE MyPlayer INSTANCE BE CREATED with 2 arguments?
# try:
# my_player=player.MyPlayer(payoff_matrix,num_iterations);
# message=message+'OK: MyPlayer instance created with 2 args.\n'
# except:
# message=message+'Error: MyPlayer instance called with 2 args cannot be created!\n'
# result = False

# CAN THE player play?
try:
my_move = my_player.play()
if CZ:
message += "OK: MyPlayer.play() běží"
else:
message += "OK: MyPlayer.play() runs smoothly."
except:
result = False
if CZ:
message += "chyba: volání MyPlayer.play() skončilo chybou"
else:
message += "Error: MyPlayer.play() does not work."
message += "\n"

# does the play method return a valid move?
try:
if game.is_valid_move(my_move):
if CZ:
message += "OK: funkce play() navrátila validní tah\n"
else:
message += 'OK: play() returns a valid move.\n'
else:
result = False
if CZ:
message += "Chyba: funkce play() navrátila nevalidní tah: "
else:
message += 'Error: output of play() is not valid move: '
message += '('+str(my_move)+')\n'
except:
pass


# CAN THE player RECORD THE OPPONENTS GESTURE?
try:
my_player.record_opponent_gesture('R')
if CZ:
message += "OK: vše v pořádku, MyPlayer.record_opponents_gesture() akceptuje soupeřovo gesto. \n"
else:
message += 'OK: MyPlayer.record_opponents_gesture() produces no error.\n'
except:
result = True # just a warning is produced
if CZ:
message += "Varování: MyPlayer.record_opponents_move() nefunguje dobře " \
"pro vlastní hru to není nezbytně třeba, ale možná se ochuzujete o možnost " \
"reakce na soupeřovu strategii \n"
else:
message += 'Warning: MyPlayer.record_opponents_move() does not work\n'

# CAN A GAME BE PLAYED WITH THE PLAYER?
try:
playerA = player.MyPlayer()
playerB = player.MyPlayer()
my_game = game.IterativeGame(playerA, playerB, num_iterations)
# run game
start_time = time.time()
errors = my_game.run()
end_time = time.time()
elapsed_time = end_time-start_time
if sum(errors)>0:
message += my_game.err_message
result = False
else:
if CZ:
message += "OK: krátká hra o %d iteracích v pořádku zahrána, trvala %2.4f sekund\n"%(num_iterations, elapsed_time)
else:
message += "OK: a short game with %d iterations has been played which took %2.4f seconds\n"%(num_iterations, elapsed_time)
if (elapsed_time < num_iterations/10): # 1 second ought to be enough for 100 iterations
if CZ:
message += "OK: váš hráč je dostatečně rychlý\n"
else:
message += "OK: your player is fast enough\n"
else:
if CZ:
message += "Varování: váš hráč je poměrně pomalý, zkuste zrychlit \n"
else:
message += "Warning: isn't you player too slow? Would you consider a speed up a bit? \n"
except RuntimeError as err:
if CZ:
message += "Chyba: krátká hra skončila chybou, odpovídající chybové hlášení viz níže: \n"
else:
message += "Error: playing a short game with the player produced error. See the error message below: \n"
"""message += '

' +traceback.format_exc()+'

'+'\n'"""
message += '

' + str(err) +'

' + '\n'
result = False

return(result,message)

def dump_points(filepath,points):
with fileutils.file_open_writable(filepath+os.sep+"results.txt") as f_out:
f_out.write("%d\n"%points)

def dump_html(filepath,message,result):
with fileutils.file_open_writable(filepath+os.sep+"results.html") as f_out:
f_out.write("\n")
f_out.write("")
if CZ:
title = "Zkouška, zda je váš hráč schopen turnaje proti ostatním"
else:
title = "Checking whether the player is able to play:"
f_out.write("
"+title+"
\n")
f_out.write("Message:
%s

\n"%message)
f_out.write("Summary: ")

if result:
if CZ:
final_message = "Všechny kontrolní body v pořádku, může hrát turnaj"
else:
final_message = "All check points passed, you may play the tournament"
else:
if CZ:
final_message = "Některé kontroly selhaly, opravte prosím problém"
else:
final_message = "Some of the check points were not passed. Plase fix the problem"
f_out.write(final_message+"\n")
# f_out.write("")


def sanitize_html_text(message):
html_message = message
# html_message=message.replace('<','<')
# html_message=html_message.replace('>','>')
html_message=html_message.replace('\n','
\n')
return(html_message)

def main():
# read the command line parameters
# first one is the absolute path to the student files
# second one is the group Id
(choices,args) = getopt.getopt(sys.argv[1:],"")
filepath = args[0]

dir_with_player=filepath+os.sep
# add path to the specified player.py to the system path:
sys.path.append(dir_with_player)

print(dir_with_player)
(result,message)=make_check()

print(result,message)

if result: # True means, it went well
points = 1
else:
points = 0
### write down the result to result.txt and result.html
# print answer,points,message

dump_points(filepath,points)
dump_html(filepath,sanitize_html_text(message),result)

if __name__ == "__main__":
main()








)
OK: vše v pořádku, MyPlayer.record_opponents_gesture() akceptuje soupeřovo gesto.
