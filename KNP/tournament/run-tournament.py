#! /usr/bin/env python3
# -*- coding: utf-8 -*-
import shutil
import os
import sys
import traceback
import argparse
sys.path.append('..'+os.sep+"share")
import game
import fileutils
import studentplayer
import tournament_table
# some config constants
TMPDIR = "/tmp"
PLAYFIELD = TMPDIR+os.sep+"playfield" # working directory for storing the players
class TournamentConfig:
    def __init__(self,tid=1):
        self.numgames = tid
        self.name = "tournament%d"%tid
        self.results_dir = TMPDIR+os.sep+self.name
        fileutils.clean_makedir(self.results_dir)
        self.fname_errors = self.results_dir+os.sep+"errors.txt"
        self.fname_duels = self.results_dir+os.sep+"game_courses.txt"
def import_player_modules(players, fielddir):
    """
    it also modifies players array - adds error message if import error
    :param players:
    :param fielddir:
    :return: hash array of imported modules - hashed by usernames
    """
    modules = {}
    for username in list(players.keys()):
        oldname = "player"
        modulename = oldname+username
        print('copy',players[username].filepath+"/"+oldname+".py",'into',fielddir+"/"+modulename+".py")
        ret_value = shutil.copyfile(players[username].filepath+"/"+oldname+".py",fielddir+"/"+modulename+".py")
        print(ret_value)
        # copying the common definitions needed just for the demo,
        # shutil.copy(players[username].filepath+"/"+"player.py",fielddir)
        # print(("importing",modulename))
        # modules[username] = __import__(modulename)
    # split the copying and importing, may help?
    # it happened on MacOSX that the import followed too much to close
    # to the copying phase and importing errors occur frequently
    for username in list(players.keys()):
        oldname = "player"
        modulename = oldname+username
        print(("importing", modulename))
        try:
            modules[username] = __import__(modulename)
        except:
            errstring = "Error when importing the player module" + traceback.format_exc()
            modules[username] = errstring
            # add record to the player
            players[username].errors.append(errstring)
            players[username].playertype = "player could not be imported - playertype unknown"
            # raise # not yet handled gracefully
    return(modules)
def prepare_duel_pairs(players,use_diversionists=False):
    pairs = []
    pos = 1
    if use_diversionists:
        usernames = list(players.keys())
    else: # exclude diversionists
        usernames = []
        for username in list(players.keys()):
            if not("diversionist" in username):
                usernames.append(username)
    # check and discard players with somerrors
    usernames_for_tournaments = []
    for username in usernames:
        if not(players[username].errors): # if no error message
            # -&gt; add only those players that can play
            usernames_for_tournaments.append(username)
    for username in usernames_for_tournaments:
        # for all of the remainining usernames eligible in tournaments
        for opponent in usernames_for_tournaments[pos:]:
            # add the pair to the list of all pairs
            pairs.append((username,opponent))
        pos = pos+1 # increment the pointer
    return(pairs)
def initialize_duelants(pair,modules,tc,players):
    duelants = []
    errs = [False,False]
    errmsg = ""
    for i in range(0,2):
        try:
            if tc.numgames is None:
                duelants.append(modules[pair[i]].MyPlayer())
            else:
                duelants.append(modules[pair[i]].MyPlayer())
        except:
            errmsg = "Problems with constructing player " + pair[i] + "\n" + traceback.format_exc()
            errs[i] = True
            duelants.append(None)
            # raise
    return(duelants, errs, errmsg)
def dump_overall_results(players):
    rankings, rankpoints = studentplayer.compute_ranking(players)
    for playername in players.keys():
        fname = players[playername].filepath+os.sep+"tournament"
        with fileutils.file_open_writable(fname+".html") as f_html:
            studentplayer.dump_ranking(rankings, rankpoints, players, f_html, 1, "html")
        with fileutils.file_open_writable(fname+".txt") as f_txt:
            f_txt.write("%d\n"%players[playername].rankpoints)
def main():
    parser = argparse.ArgumentParser()
    # read the first parameters
    parser.add_argument("players_dir", help="path to the players where username/player.py is expected")
    # and just swallow gently the rest
    parser.add_argument("arg-remainder", nargs=argparse.REMAINDER)
    args = parser.parse_args()
    # initialize playfield
    fileutils.clean_makedir(PLAYFIELD)
    sys.path.append(PLAYFIELD)
    # read and intialize the players again
    # players hash array of Player objects, hashed by the username
    players = studentplayer.read_usernames(args.players_dir)
    modules = import_player_modules(players, PLAYFIELD)
    pairs = prepare_duel_pairs(players)
    tc = TournamentConfig(37)
    tournament_stats = tournament_table.TournamentTable(tc)
    f_err = fileutils.file_open_writable(tc.fname_errors)
    f_course = fileutils.file_open_writable(tc.fname_duels)
    print("playing games ...")
    game_counter = 0
    for pair in pairs:
        game_counter += 1
        print("duel ", game_counter, ": playing ", pair[0], " vs ", pair[1])
        duelants, errs, errmsg = initialize_duelants(pair, modules, tc, players)
        duel = game.IterativeGame(duelants[0], duelants[1], tc.numgames, pair)
        if any(err for err in errs):
            # numerrors = duel.run_void(errmsg,errs)
            numerrors = duel.run()
        else:
            numerrors = duel.run()
        # print(numerrors)
        duel.dump_errors(f_err)
        duel.dump_course(f_course)
        payoffs = duel.get_players_payoffs()
        # remember all duels for later analysis
        # hashed by the (username1,username2) pair
        tournament_stats.add_duel(pair,duel)
        # add the payoffs to the overall profits
        for i in range(0,2):
            players[pair[i]].adderror(numerrors[i])
            players[pair[i]].addprofit(payoffs[i])
            players[pair[i]].addgames(tc.numgames)
            # and it also does not handle completely the case if the player is defect
            if not(players[pair[i]].playertype):
                players[pair[i]].playertype = duelants[i].__doc__
    f_err.close()
    f_course.close()
    dump_overall_results(players)
if __name__ == "__main__":
    main()