__author__ = 'svoboda'

# this code is not used for the game at the moment
# the idea was to pass an object to each player
# that would change the object property
class HandSign:
    def __init__(self):
    self._sign = None

    @property
    def sign(self):
        '''
        the current hand sign
        :return:
        '''
        return self._sign

    @sign.setter
    def sign(self, value):
        if value in ['R', 'P', 'S']:
        self._sign = value
        else:
        raise AttributeError

    def __eq__(self, other):
        return self.sign == other.sign

    def __gt__(self, other):
        return [self.sign,other.sign] in [['P','R'],['S','P'],['R','S']]

    def __bool__(self):
    return self.sign in ['R','P','S']
    )
    OK: vše v pořádku, MyPlayer.record_opponents_gesture() akceptuje soupeřovo gesto.
