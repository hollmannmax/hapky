import player1
import player2

p1 = player1.MyPlayer()
p2 = player2.MyPlayer()

for i in range(5):
    if hasattr(p1, 'name'):
        print('p1 the one')
    if hasattr(p2, 'name'):
        print('p2 the one')
    print(p1.play())
    print(p2.play())
