__author__ = 'svoboda'

import playertom
import playerdummy

def evaluate_moves(moves):
    '''
    compares moves (plays) and decides about the winner
    :param moves: 1x2 list of valid moves
    :return: 1x2 list with points [1,0] or [0,1] depending on who is winner
    '''
    if moves in [['P','R'],['S','P'],['R','S']]:
        return [1,0]
    else:
        return [0,1]

def is_valid_move(move):
    '''
    check whether it is a valid move
    :param move: string
    :return: True or False
    '''
    return move in ['P','R','S']

class Game:
    def __init__(self,p1,p2):
        self.p1 = p1
        self.p2 = p2
        self.winner = None

    def run(self):
        draw = True
        while draw:
            move1 = self.p1.play()
            move2 = self.p2.play()
            draw = (move1 == move2)
            print('player 1 played [',move1,'-',move2,'] was played by player 2')
        result = evaluate_moves([move1,move2])
        if result[0]>result[1]:
            self.winner = p1
            return 0
        else:
            self.winner = p2
            return 1


class IterativeGame:
    def __init__(self,p1,p2,runs=1):
        '''
        game constructor
        :param p1: pointer to player 1 object
        :param p2: pointer to player 2 object
        :param runs: how many runs (iterations)
        :return: pointer to a game object
        '''
        self.runs = runs
        self.p = [p1,p2]
        self.profits = [0,0]

    def run(self):
        for k in range(self.runs):
            draw = True
            while draw:
                moves = [None,None] # init moves
                for i in range(2):
                    moves[i] = self.p[i].play()
                    if not(is_valid_move(moves[i])):
                        raise RuntimeError # will be handled later, more gracefully
                draw = (moves[0] == moves[1])
            profit_increments = evaluate_moves(moves)
            for i in range(2):
                self.profits[i] += profit_increments[i]


def compute_stats(winners):
    wins = [0,0]
    for winner in winners:
        wins[winner] = wins[winner]+1
    return wins

if __name__ == "__main__":
    p1 = playertom.MyPlayer()
    p2 = playerdummy.MyPlayer()
    winners = []
    for i in range(10):
        g = Game(p1,p2)
        winners.append(g.run())
    print('Winner is:',g.winner.__doc__)
    wins = compute_stats(winners)
    print(p1.__doc__, 'won %d times'%wins[0])
    print(p2.__doc__, 'won %d times'%wins[1])

    g = IterativeGame(p1,p2,10)
    g.run()
    print(g.profits)
    print('OK: vše v pořádku, MyPlayer.record_opponents_gesture() akceptuje soupeřovo gesto.')
