import random
__author__ = 'Adam Dejl'
#result of proof of concept error-based python injection
class MyPlayer:
    '''
    __author__ = 'Adam Dejl'
    '''
    def __init__(self):
        random.seed()
        self.seeds = [5, 24, 794, 71, 3, 58, 24, 68, 89, 74, 56, 14, 2, 378, 245]
        random.seed(self.seeds[random.randint(0, 14)])
        self.opponent_last_gesture = None # na zacatku zadne
        self.my_last_gesture = None
        self.last_match_result = None
        self.protitahy = {'R': 'P', 'P': 'S', 'S': 'R'}
        self.game_count = 0
        self.games_lost = 0
        self.games_won = 0
        self.played_strategy = "video"
        self.won_of_last_five_games = 0

    def random_strategy_gesture(self):
        # Strategie vybírající náhodný tah
        rand = random.randint(1, 3)
        if rand == 1:
            tah = 'R'
        elif rand == 2:
            tah = 'P'
        elif rand == 3:
            tah = 'S'
        return tah

    def video_strategy_gesture(self):
        # Strategie tupe kopirujici strategii z videa
        if self.opponent_last_gesture == None or self.last_match_result == 'D':
            # Zcela nahodna hra na zacatku a pri remize v posledni hre
            rand = random.randint(1, 3)
            if rand == 1:
                tah = 'R'
            elif rand == 2:
                tah = 'P'
            elif rand == 3:
                tah = 'S'
        elif self.last_match_result == 'W':
            tah = self.opponent_last_gesture
        elif self.last_match_result == 'L':
            tah = self.protitahy[self.opponent_last_gesture]
        return tah

    def countervideo_strategy_gesture(self):
        # Strategie zamerena proti strategii z videa
        if self.opponent_last_gesture == None or self.last_match_result == 'D':
            # Zcela nahodna hra na zacatku a pri remize v posledni hre
            rand = random.randint(1, 3)
            if rand == 1:
                tah = 'R'
            elif rand == 2:
                tah = 'P'
            elif rand == 3:
                tah = 'S'
        elif self.last_match_result == 'W':
            tah = self.protitahy[self.protitahy[self.my_last_gesture]]
        elif self.last_match_result == 'L':
            tah = self.protitahy[self.my_last_gesture]
        return tah

    def play(self):
        '''
        zakladni hraci funkce
        :return:
        'R' nebo 'P' nebo 'S'
        pismenka odpovidaji anglickemu Rock, Paper, Scissors
        '''
        if self.opponent_last_gesture != None:
            if self.my_last_gesture == self.opponent_last_gesture:
                self.last_match_result = 'D'
            elif self.my_last_gesture == 'R' and self.opponent_last_gesture == 'P':
                self.last_match_result = 'L'
                self.games_lost += 1
            elif self.my_last_gesture == 'R' and self.opponent_last_gesture == 'S':
                self.last_match_result = 'W'
                self.games_won += 1
                self.won_of_last_five_games += 1
            elif self.my_last_gesture == 'S' and self.opponent_last_gesture == 'R':
                self.last_match_result = 'L'
                self.games_lost += 1
            elif self.my_last_gesture == 'S' and self.opponent_last_gesture == 'P':
                self.last_match_result = 'W'
                self.games_won += 1
                self.won_of_last_five_games += 1
            elif self.my_last_gesture == 'P' and self.opponent_last_gesture == 'S':
                self.last_match_result = 'L'
                self.games_lost += 1
            elif self.my_last_gesture == 'P' and self.opponent_last_gesture == 'R':
                self.last_match_result = 'W'
                self.games_won += 1
                self.won_of_last_five_games += 1
        if self.game_count > 0 and self.game_count % 3 == 0:
            if self.won_of_last_five_games < 2:
                if self.played_strategy == "video":
                    self.played_strategy = "countervideo"
                elif self.played_strategy == "countervideo":
                    self.played_strategy = "random"
            self.won_of_last_five_games = 0
        if self.played_strategy == "video":
            tah = self.video_strategy_gesture()
        elif self.played_strategy == "countervideo":
            tah = self.countervideo_strategy_gesture()
        elif self.played_strategy == "random":
            tah = self.random_strategy_gesture()
        self.my_last_gesture = tah
        self.game_count += 1
        return tah

    def record_opponent_gesture(self,gesture):
        '''
        zaznamena gesto soupere v prave dokoncene hre
        :param gesture: 'R' or 'P' or 'S'
        :return: neni specifikovano
        '''
        # ulozeni gest do pameti hrace
        self.opponent_last_gesture = gesture

if __name__ == "__main__": # nasledujici kod bude vykonan, pokud bude player.py spusten jako program
    hrac = MyPlayer() # vytvoreni hrace
    gesto = hrac.play() # hrac je vyzvan k tomu, aby hral
    print("Vzorovy hrac hral: "+gesto)