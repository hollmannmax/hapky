(__author__ = 'svoboda'

def evaluate_moves(moves):
    '''
    compares moves (plays) and decides about the winner
    :param moves: 1x2 list of valid moves
    :return: 1x2 list with points [1,0] or [0,1] depending on who is winner
    '''
    result = [1,1]
    compare_moves = True
    for i in range(len(moves)):
        if not(is_valid_move(moves[i])):
            compare_moves = False
            result[i] = result[i]-1
    if compare_moves:
        if moves in [['P','R'],['S','P'],['R','S']]:
            return [1,0]
        else:
            return [0,1]
    else:
        return result
def is_valid_move(move):
    '''
    check whether it is a valid move
    :param move: string
    :return: True or False
    '''
    return move in ['P','R','S']
class Game:
    def __init__(self,p1,p2):
        self.p1 = p1
        self.p2 = p2
        self.winner = None
    def run(self):
        draw = True
        while draw:
            move1 = self.p1.play()
            move2 = self.p2.play()
            draw = (move1 == move2)
            print('player 1 played [',move1,'-',move2,'] was played by player 2')
        result = evaluate_moves([move1,move2])
        if result[0]&gt;result[1]:
            self.winner = p1
            return 0
        else:
            self.winner = p2
            return 1
class IterativeGame:
    def __init__(self,p1,p2,runs=1,pair=("p1","p2")):
        '''
        game constructor
        :param p1: pointer to player 1 object
        :param p2: pointer to player 2 object
        :param runs: how many runs (iterations)
        :return: pointer to a game object
        '''
        self.runs = runs
        self.p = [p1,p2]
        self.pair = pair # textual identification of the players
        self.players_payoff = [0,0]
        self.errors = [False, False]
        self.err_message = ""
        self.numerrors = [0,0]
        self.move_history = []
    def run(self):
        for k in range(self.runs):
            draw = True
            count = 0
            profit_increments = [0,0]
            while draw and count&lt;10:
                count += 1
                moves = [None,None] # init moves
                for i in range(2):
                    try:
                        moves[i] = self.p[i].play()
                    except:
                        self.err_message += "player %d did something wrong, play method crashed\n"%(i)
                    if not(is_valid_move(moves[i])):
                        self.errors[i] = True
                        self.err_message += "{} is not a valid move\n".format(moves[i])
                        self.numerrors[i] += 1
                        # raise RuntimeError("{} is not a valid move".format(moves[i])) # will be handled later, more gracefully
                draw = (moves[0] == moves[1])
                try:
                    self.p[0].record_opponent_gesture(moves[1])
                except:
                    pass
                try:
                    self.p[1].record_opponent_gesture(moves[0])
                except:
                    pass
            if draw: # still draw after many trials
                profit_increments = [0,0]
            else:
                profit_increments = evaluate_moves(moves)
            for i in range(2):
                self.players_payoff[i] += profit_increments[i]
            self.move_history.append((str(moves[0]), str(moves[1]), profit_increments[0], profit_increments[1]))
        return self.numerrors
    def get_players_payoffs(self):
        # INPUTS: none
        # OUTPUTS:
        #     a 2-element list with playerA's payoff and playerB's payoff
        return([self.players_payoff[0], self.players_payoff[1]])
        # note that this would be [None, None] if the game has not been run.
    def dump_errors(self, f_out):
        # print(self.errors)
        if any(err for err in self.errors):
            f_out.write("*** begin error desc *** \n")
            f_out.write("Duel: %s vs. %s \n"%self.pair)
            for err in range(0,len(self.errors)):
                if self.errors[err]:
                    f_out.write("Error caused by player %s \n"%self.pair[err])
            # traceback.print_exc(file=f_out)
            f_out.write("%s"%self.err_message)
            f_out.write("*** end error desc *** \n\n")
        else:
            f_out.write("No error during the Duel: %s vs. %s \n"%self.pair)
    def dump_course(self,f_out):
        f_out.write("*****************************\n")
        f_out.write("Duel {:s} vs {:s}\n".format(*self.pair))
        f_out.write("A B {:>4s} {:>4s}\n".format("Ar","Br"))
        for i in range(0,len(self.move_history)):
            # do not forget to unpack the n-tuple
            # http://stackoverflow.com/questions/539066/return-a-tuple-of-arguments-to-be-fed-to-string-format
            # f_out.write(str(self.move_history[i]))
            f_out.write("{:s} {:s} {:4d} {:4d}\n".format(*self.move_history[i]))
        # print the total sums
        f_out.write("T:  {:4d} {:4d}\n".format(*self.players_payoff))
def compute_stats(winners):
    wins = [0,0]
    for winner in winners:
        wins[winner] = wins[winner]+1
    return wins
if __name__ == "__main__":
    import playertom
    import playerdummy
    p1 = playertom.MyPlayer()
    p2 = playerdummy.MyPlayer()
    winners = []
    for i in range(10):
        g = Game(p1,p2)
        winners.append(g.run())
        print('Winner is:',g.winner.__doc__)
    wins = compute_stats(winners)
    print(p1.__doc__, 'won %d times'%wins[0])
    print(p2.__doc__, 'won %d times'%wins[1])