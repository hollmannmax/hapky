__author__ = 'Hollmann'


import sys
import time
import multiprocessing as mp


try:
    filename = sys.argv[1]
except:
    filename = "zadani2.txt"


def guess(line, line2, start, end, unique):
    """returns a list of start and end adresses which oblige
    to the rules in one line
    
    keyword:
    line -- input line of houses to run on
    """
    # 
    colors = set(line)
    n = 1
    lenght = len(line)
    for start_index in range(start, end):
        if line[start_index] in colors: colors.remove(line[start_index])
        n = max(n, start_index + 1)
        #if start_index + 1> n: n = start_index + 1
        if not line[start_index] in line[start_index + 1:n + 1]:
            for end_index in range(n, lenght):
                if line[start_index] == line[end_index]:
                    n = end_index
                    break
                if line[end_index] in colors:
                    colors.remove(line[end_index])
                    if not colors:
                        # yield((start_index + 1, end_index + 1))
                        confirm((start_index + 1, end_index + 1), line2, unique)
                        colors.add(line[start_index])
                        #n = end_index
                        break
            else:
                return
    return


def confirm(address, array, unique):
    """validates if given houses match the requirements
    
    keywords:
    adresses -- list of start and end houses to check
    array    -- original string of houses to check on
    """
    # If all house colors are to be in the subset, then the lenght of
    # set should be same for subset and whole string. If outer houses
    # have unique colors, the subset without them should be smaler by 2.
    # array_set = len(set(array))
    if len(set(array[address[0] - 1:address[1]])) == unique:
        if len(set(array[address[0]:address[1] - 1])) == unique - 2:
            print((address[0], address[1]))


if __name__ == '__main__':
    start_time = time.time()
    with open(filename) as file_object:
        lines = file_object.read().splitlines()
    print(filename)
    lenght = int(lines[0])
    print(lenght)
    # confirm(guess(lines[1]), lines[2])
    cpu_count = mp.cpu_count()
    jobs = []
    unique = len(set(lines[2]))
    for i in range(cpu_count):
        p = mp.Process(target=guess, args=(lines[1], lines[2], int(lenght * i / cpu_count), int(lenght * (i + 1) / cpu_count), unique))
        jobs.append(p)
        p.start()
    for job in jobs:
        job.join()
    # guess(lines[1], lines[2])
    print("--- %s seconds ---" % (time.time() - start_time))
