__author__= 'Snooli'

import time

filename = "zadani2" + ".txt"

def guess(line):
    colors = set(line)
    set_len = len(colors) - 1
    n = set_len
    lenght = len(line)
    for start_index in range(lenght - set_len):
        if line[start_index] in colors: colors.remove(line[start_index])
        n = max(n, start_index + 1)
        #if start_index + 1> n: n = start_index + 1
        for end_index in range(n, lenght):
            if line[start_index] in line[start_index + 1:end_index + 1]:
                n = end_index
                break
            if line[end_index] in colors:
                colors.remove(line[end_index])
                if not colors:
                    yield [start_index + 1, end_index + 1]
                    colors.add(line[start_index])
                    break

def confirm(adresss, array):
    array_set = len(set(array))
    for adress in adresss:
        if len(set(array[adress[0] - 1:adress[1]])) == array_set:
            if len(set(array[adress[0]:adress[1] - 1])) == array_set - 2:
                yield [adress[0], adress[1]]

if __name__ == '__main__':
    start_time = time.time()
    with open(filename) as file_object:
        lines = file_object.read().splitlines()
    results = confirm(confirm(guess(lines[1]), lines[2]), lines[1])
    for result in results:
        print(result)
    print("--- %s seconds ---" % (time.time() - start_time))
