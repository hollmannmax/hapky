use std::fs::File;
use std::io::BufReader;
use std::io::prelude::*;
use std::cmp::max;
use std::env;
use std::time::Instant;
use std::str::Chars;

struct Address(usize, usize);

fn main() -> std::io::Result<()> {
    let now = Instant::now();
    let args: Vec<String> = env::args().collect();
    let mut path: String = "domecky/".to_owned();
    path.push_str(&args[1]);
//    let path = "/home/max/hapky/domecky/zadani1.txt";
    println!("{}", &args[1]);
    let file = File::open(path)?;
    let mut buf_reader = BufReader::new(file);
    let mut contents = String::new();
    buf_reader.read_to_string(&mut contents)?;
    let mut lines= contents.lines();
    let length: usize = match lines.next(){
        Some(x) => x.parse().unwrap(),
        None => panic!("not enough lines!"),
    };
    let line1 = lines.next().unwrap();
    let line2 = lines.next().unwrap();
    println!("{}", length);
    let table = gen_table();
    let colors1 = hash(line1.chars(), table);
    let colors2 = hash(line2.chars(), table);
    let addresses = match guess(line1.chars().collect(), length, colors1.clone(), table){
        Some(x) => x,
        None => panic!("No solution found!")
    };
    let results = match confirm(addresses, line2.chars().collect(), colors2, table) {
        Some(x) => x,
        None => panic!("No solution found!")
    };
    for result in results.iter(){
        println!("{}, {}", result.0, result.1);
    }
    println!("{}.{:>09}s", now.elapsed().as_secs(), now.elapsed().subsec_nanos());
    Ok(())
}

fn guess(line: Vec<char>, length: usize, colors: usize, table: [usize; 256]) -> Option<Vec<Address>> {
    let mut result: Vec<Address> = Vec::new();
    let mut n: usize = 1;
    let mut set: usize = colors;
    for start in 0..length-1{
        if set & table[line[start] as usize] != 0{
//        if set.contains(&line[start]){
            set &= !table[line[start] as usize];
//            set.remove(&line[start]);
        }
        n = max(start + 1, n);
        if !line[(start + 1)..(n + 1)].contains(&line[start]){
            for end in n..length{
                if line[start] == line[end]{
                    n = end;
                    break
                }
                else if set & table[line[end] as usize] != 0{
//                else if set.contains(&line[end]){
                    set &= !table[line[end] as usize];
//                    set.remove(&line[end]);
                    if set == 0{
//                    if set.is_empty() {
                        result.push(Address(start + 1, end + 1));
//                        println!("{}, {}", start +1, end + 1);
                        set |= table[line[start] as usize];
//                        set.insert(line[start]);
                        break
                    }
                }
            }
        }
    }
    if result.len() == 0 {
        None
    }else{
        Some(result)
    }
}

fn confirm(addresses: Vec<Address>, line: Vec<char>, colors: usize, table: [usize; 256]) -> Option<Vec<Address>> {
    let mut result: Vec<Address> = Vec::new();
    for address in addresses{
        let mut set = colors.clone();
        for l in line[address.0 .. address.1 -1].iter(){
            set &= !table[*l as usize];
        }
        if table[line[address.0 -1] as usize] != table[line[address.1 -1] as usize]{
            if set == table[line[address.0 -1] as usize] | table[line[address.1 -1] as usize] {
//                println!("{}, {}", address.0, address.1);
                result.push(address);
            }
        }
    }
    if result.len() == 0 {
        None
    }else{
        Some(result)
    }
}

fn hash(line: Chars, table: [usize; 256]) -> usize{
    let mut result: usize = 0;
    for l in line{
        result |= table[l as usize];
    }
    result
}

fn gen_table() -> [usize; 256]{
    let mut result: [usize; 256] = [0; 256];
    let mut c: usize = 1;
    for n in 'a' as usize ..'z' as usize +1{
        result[n as usize] = c;
        c <<= 1;
    }
    for n in 'A' as usize ..'Z' as usize +1{
        result[n as usize] = c;
        c <<= 1;
    }
    for n in '0' as usize ..'9' as usize +1{
        result[n as usize] = c;
        c <<= 1;
    }
    result
}