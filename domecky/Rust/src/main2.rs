use std::fs::File;
use std::io::BufReader;
use std::io::prelude::*;
use std::cmp::max;
use std::env;
use std::time::Instant;
use std::str::Chars;
use std::thread;
use std::sync::Arc;

struct Address(usize, usize);

fn main() -> std::io::Result<()> {
    let cpu_count: usize = 6;
    let now = Instant::now();
    let args: Vec<String> = env::args().collect();
    let mut path: String = "domecky/".to_owned();
    path.push_str(&args[1]);
//    let path = "/home/max/hapky/domecky/zadani1.txt";
    println!("{}", &args[1]);
    let file = File::open(path)?;
    let mut buf_reader = BufReader::new(file);
    let mut contents: String = String::new();
    buf_reader.read_to_string(&mut contents)?;
    let mut lines = contents.lines();
    let length = Arc::new(lines.next().unwrap().parse().unwrap());
    let liine1: &str = lines.next().unwrap();
    let liine2: &str = lines.next().unwrap();
    println!("{}", *length);
    let table: Arc<[usize; 256]> = Arc::new(gen_table());
    let colors1: Arc<usize> = Arc::new(hash(liine1.chars(), *table));
    let colors2: Arc<usize> = Arc::new(hash(liine2.chars(), *table));
    let line1: Arc<Vec<char>> = Arc::new(liine1.chars().collect());
    let line2: Arc<Vec<char>> = Arc::new(liine2.chars().collect());
    let mut children = vec![];
    for i in 0..cpu_count {
        let line1 = Arc::clone(&line1);
        let length = Arc::clone(&length);
        let colors1 = Arc::clone(&colors1);
        let table = Arc::clone(&table);
        let line2 = Arc::clone(&line2);
        let colors2 = Arc::clone(&colors2);
        children.push(thread::spawn(move || {
            match guess(&*line1, &*length, &*colors1, &*table,
                  (*length - 1) * i / cpu_count, (*length - 1) * (i + 1) / cpu_count){
                Some(x) => confirm(x, &*line2, &*colors2, &*table),
                None => None
            }
        }));
    }
    let mut results: Vec<Address> = vec![];
    for child in children {
        let mut foo = child.join();
        if foo.is_ok(){
            match foo.ok().unwrap(){
                Some(mut x) => results.append(&mut x),
                None => ()
            };
        }
    }
    for result in results.iter(){
        println!("{}, {}", result.0, result.1);
    }
    println!("{}.{:>09}s", now.elapsed().as_secs(), now.elapsed().subsec_nanos());
    Ok(())
}

fn guess(line: &Vec<char>, length: &usize, colors: &usize, table: &[usize; 256], start: usize, end: usize) -> Option<Vec<Address>> {
    let mut result: Vec<Address> = Vec::new();
    let mut n: usize = 1;
    let mut set: usize = colors.clone();
    for start_index in start..end{
        if set & table[line[start_index] as usize] != 0{
            set &= !table[line[start_index] as usize];
        }
        n = max(start_index + 1, n);
        if !line[(start_index + 1)..(n + 1)].contains(&line[start_index]){
            for end_index in n..*length{
                if line[start_index] == line[end_index]{
                    n = end_index;
                    break
                }
                else if set & table[line[end_index] as usize] != 0{
                    set &= !table[line[end_index] as usize];
                    if set == 0{
                        result.push(Address(start_index + 1, end_index + 1));
                        set |= table[line[start_index] as usize];
                        break
                    }
                }
            }
        }
    }
    if result.len() == 0 {
        None
    }else{
        Some(result)
    }
}

fn confirm(addresses: Vec<Address>, line: &Vec<char>, colors: &usize, table: &[usize; 256]) -> Option<Vec<Address>> {
    let mut result: Vec<Address> = Vec::new();
    for address in addresses{
        let mut set = colors.clone();
        for l in line[address.0 .. address.1 -1].iter(){
            set &= !table[*l as usize];
        }
        if table[line[address.0 -1] as usize] != table[line[address.1 -1] as usize]{
            if set == table[line[address.0 -1] as usize] | table[line[address.1 -1] as usize] {
                result.push(address);
            }
        }
    }
    if result.len() == 0 {
        None
    }else{
        Some(result)
    }
}

fn hash(line: Chars, table: [usize; 256]) -> usize{
    let mut result: usize = 0;
    for l in line{
        result |= table[l as usize];
    }
    result
}

fn gen_table() -> [usize; 256]{
    let mut result: [usize; 256] = [0; 256];
    let mut c: usize = 1;
    for n in 'a' as usize ..'z' as usize +1{
        result[n as usize] = c;
        c <<= 1;
    }
    for n in 'A' as usize ..'Z' as usize +1{
        result[n as usize] = c;
        c <<= 1;
    }
    for n in '0' as usize ..'9' as usize +1{
        result[n as usize] = c;
        c <<= 1;
    }
    result
}