__author__ = 'Hollmann'


import sys, time


try:
    filename = sys.argv[1]
except:
    filename = "zadani2.txt"


def guess(line):
    """returns a list of start and end adresses which oblige
    to the rules in one line
    
    keyword:
    line -- input line of houses to run on
    """
    # 
    colors = set(line)
    result = []
    n = 1
    lenght = len(line)
    for start_index in range(lenght):
        if line[start_index] in colors: colors.remove(line[start_index])
        n = max(n, start_index + 1)
        #if start_index + 1> n: n = start_index + 1
        if not line[start_index] in line[start_index + 1:n + 1]:
            for end_index in range(n, lenght):
                if line[start_index] == line[end_index]:
                    n = end_index
                    break
                if line[end_index] in colors:
                    colors.remove(line[end_index])
                    if not colors:
                        result.append((start_index + 1, end_index + 1))
                        colors.add(line[start_index])
                        #n = end_index
                        break
            else:
                return result
    return result


def confirm(adresses, array):
    """validates if given houses match the requirements
    
    keywords:
    adresses -- list of start and end houses to check
    array    -- original string of houses to check on
    """
    # If all house colors are to be in the subset, then the lenght of
    # set should be same for subset and whole string. If outer houses
    # have unique colors, the subset without them should be smaler by 2.
    array_set = len(set(array))
    result = []
    for address in adresses:
        if len(set(array[address[0] - 1:address[1]])) == array_set:
            if len(set(array[address[0]:address[1] - 1])) == array_set - 2:
                result.append((address[0], address[1]))
    return result


if __name__ == '__main__':
    start_time = time.time()
    with open(filename) as file_object:
        lines = file_object.read().splitlines()
    print(filename)
    print(lines[0])
    results = confirm(guess(lines[1]), lines[2])
    for result in results:
        print(result)
    print("--- %s seconds ---" % (time.time() - start_time))
