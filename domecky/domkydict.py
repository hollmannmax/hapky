__author__= 'Snooli'


import sys, time


try:
    filename = sys.argv[1]
except:
    filename = "random.txt"


def guess(line):
    colors = set(line)
    set_len = len(colors) - 1
    result = []
    n = 1
    length = len(line)
    letters = {}
    for l in colors:
        letters[l] = 0
    letters[line[0]] = 1
    for start_index in range(length - set_len):
        if start_index + 1 > n:
            letters[line[n]] += 1
            n = start_index + 1
        for end_index in range(n, length):
            letters[line[end_index]] += 1
            if letters[line[start_index]] > 1:
                break
            if letters[line[end_index]] == 1:
                if all(x > 0 for x in letters.values()):
                    result.append((start_index + 1, end_index + 1))
                    break
        else:
            return result
        n = end_index
        letters[line[end_index]] -= 1
        letters[line[start_index]] -= 1
    return result

#from dictguess import guess
#from confirm import confirm

def confirm(adresss, array):
    array_set = len(set(array))
    result = []
    for address in adresss:
        if len(set(array[address[0] - 1:address[1]])) == array_set:
            if len(set(array[address[0]:address[1] - 1])) == array_set - 2:
                result.append((address[0], address[1]))
    return result


if __name__ == '__main__':
    start_time = time.time()
    with open(filename) as file_object:
        lines = file_object.read().splitlines()
    print(filename)
    print(lines[0])
    results = confirm(confirm(guess(lines[1]), lines[2]), lines[1])
    for result in results:
        print(result)
    print("--- %s seconds ---" % (time.time() - start_time))
