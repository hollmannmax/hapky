import random, string, sys
PATH_ASSIGNMENT = 'random5' + '.txt'
try:
    N = int(sys.argv[1])
except:
    N = 10**3

content = str(N)
for _ in range(2):
    content += ('\n')
    #content += (''.join(random.choice(string.ascii_lowercase + string.ascii_uppercase) for _ in range(N)))
    content += (''.join(random.choice(string.ascii_uppercase) for _ in range(N)))
with open(PATH_ASSIGNMENT, 'w') as file:
    file.write(content)
print('done')
