__author__= 'Snooli'


import sys, time
from typing import List, Set, Tuple


try:
    filename = sys.argv[1]
except:
    filename = "random.txt"


def guess(line):
    colors: Set[str] = set(line)
    set_len: int = len(colors) - 1
    result: List[Tuple[int]] = []
    n: int = set_len
    lenght: int = len(line)
    used = set()
    for start_index in range(lenght - set_len):
        if line[start_index] in colors:
            colors.remove(line[start_index])
        #n = max(n, start_index + 1)
        if start_index + 1> n:
            n = start_index + 1
        for end_index in range(n, lenght):
            used.add(end_index)
            if line[start_index] in line[start_index + 1:end_index + 1]:
                n = end_index
                break
            if line[end_index] in colors:
                colors.remove(line[end_index])
                if not colors:
                    result.append((start_index + 1, end_index + 1))
                    colors.add(line[start_index])
                    #n = end_index
                    break
        else:
            return result
    return result

#from oldguess import guess
#from confirm import confirm

def confirm(adresses, array):
    array_set = len(set(array))
    result = []
    for address in adresses:
        if len(set(array[address[0] - 1:address[1]])) == array_set:
            if len(set(array[address[0]:address[1] - 1])) == array_set - 2:
                result.append((address[0], address[1]))
    return result


if __name__ == '__main__':
    start_time = time.time()
    with open(filename) as file_object:
        lines = file_object.read().splitlines()
    #results = confirm(guess(lines[1]), lines[2])
    print(filename)
    print(lines[0])
    results = confirm(guess(lines[1]), lines[2])
    for result in results:
        print(result)
    print("--- %s seconds ---" % (time.time() - start_time))
