__author__ = 'Sheemon'

import copy, time

PATH_ASSIGNMENT = "random.txt"
#PATH_RESULT = "reseni2" + PATH_ASSIGNMENT + ".txt"

def load(path):
    with open(path, 'r', encoding = 'utf-8') as file:
        number = file.readline()
        house_colours = file.readline()
        roofs_colours = file.readline()
        number = number.rstrip("\n")
        house_colours = house_colours.rstrip("\n")
        roofs_colours = roofs_colours.rstrip("\n")
    return number, house_colours, roofs_colours

def compute(house_colours, roofs_colours):

    house_colour_set = get_colour_set(house_colours)
    roofs_colour_set = get_colour_set(roofs_colours)
    house_results = get_results(house_colours, house_colour_set)
    roofs_results = get_results(roofs_colours, roofs_colour_set)
    final_result = get_intersection(house_results, roofs_results)

    return final_result

def get_results(colours, colour_set):
    result = []
    for start_index in range(len(colours)):
        used_colours = copy.deepcopy(colour_set)
        used_colours.remove(colours[start_index])
        for end_index in range(start_index + 1, len(colours)):
            if colours[start_index] == colours[end_index]:
                break
            used_colours = [x for x in used_colours if x != colours[end_index]]
            if len(used_colours) == 0:
                result.append([start_index + 1, end_index + 1])
                break
    return result

def get_colour_set(colours):
    #set = []
    #for colour in colours:
    #    if not colour in set:
    #        set.append(colour)
    #return set
    return list(set(colours))

def get_intersection(list1, list2):
    intersection = []
    for a in list1:
        if a in list2:
            intersection.append(a)
    return intersection

if __name__ == '__main__':
    start_time = time.time()
    number, house_colours, roofs_colours = load(PATH_ASSIGNMENT)
    print ("ahoj")
    results = compute(house_colours, roofs_colours)
    for result in results:
            print(result)
    print("--- %s seconds ---" % (time.time() - start_time))
    '''with open(PATH_RESULT, 'w') as file:
        for result in results:
            print(results)
            for num in result:
                file.write(str(num) + " ")
            file.write("\n")'''

