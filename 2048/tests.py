# 2048: Task 2 - Line
# This module contains test support, it is not part of the solution.

from simulator import numbers_to_left

TEST_CASES_FPATH = 'rows_complete.txt'

def load_test_cases(fpath):
    """Load test cases from text file.

    Assume format 'n n n n : n n n n : n' on each line.
    """
    tcs = []
    with open(fpath, 'rt', encoding='utf-8') as f:
        for line in f:
            tcs.append(parse_line(line))
    return tcs

def parse_line(line):
    """Parse line with the format 'n n n n : n n n n : n', where n is an int.

    Assume correctly formated input.
    """
    parts = line.split(':')
    in_row = parse_numbers(parts[0])
    exp_out_row = parse_numbers(parts[1])
    exp_score = parse_numbers(parts[2])[0]
    return in_row, exp_out_row, exp_score


def parse_numbers(str):
    """Parse numbers from str, expecting format 'n n n n'.

    Assume correctly formated input.
    """
    str_numbers = str.strip().split()
    numbers = [int(x) for x in str_numbers]
    return numbers

if __name__=='__main__':
    # Load test cases from file
    tcs = load_test_cases(TEST_CASES_FPATH)
    # For each test case:
    for in_row, exp_out_row, exp_score in tcs:
        err = False
        print('\nTesting {}: '.format(str(in_row)), end='')
        # Call your function to learn its outputs
        obs_out_row, obs_score = numbers_to_left(in_row)
        # Check the resulting row
        if obs_out_row != exp_out_row:
            print('\n- Error: expected row {}, but your function produced {}.'.format(
                str(exp_out_row), str(obs_out_row)), end='')
            err = True
        # Check the resulting score
        if obs_score != exp_score:
            print('\n- Error: expected score {}, but your function produced {}.'.format(
                str(exp_score), str(obs_score)), end='' )
            err = True
        if not err:
            print('OK', end='')