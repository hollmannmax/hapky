def numbers_to_left(in_row):
    """Move numbers in a row to the left, according to 2048 rules"""

    # Initialize output row configuration to 4 zeros
    out_row = [0 for x in in_row]
    # Initialize score
    score = 0
    #
    # ... add your code here ...
    #
    result = []
    hold = 0
    for x in in_row:
        if x == 0:
            pass
        elif hold == 0:
            hold = x
        elif hold == x:
            result.append(x + x)
            score += x + x
            hold = 0
        else:
            result.append(hold)
            hold = x
    result.append(hold)
    for x in range(len(in_row) - len(result)):
        result.append(0)


    return result, score
