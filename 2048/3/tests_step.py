# 2048: Task 3 - Step
# This module contains test support, it is not part of the solution.

from simulator import move_tiles
from testcases import tcs

# For each test case:
for in_board, direction, exp_out_board, exp_score in tcs:
    err = False
    print('\nTesting move_tiles({}, "{}"): '.format(str(in_board),direction), end='')
    # Call your function to learn its outputs
    obs_out_board, obs_score = move_tiles(in_board, direction)
    # Check the resulting board
    if obs_out_board != exp_out_board:
        print('\n- Error: expected board {}, but your function produced {}.'.format(
            str(exp_out_board), str(obs_out_board)), end='')
        err = True
    # Check the resulting score
    if obs_score != exp_score:
        print('\n- Error: expected score {}, but your function produced {}.'.format(
            str(exp_score), str(obs_score)), end='' )
        err = True
    if not err:
        print('OK', end='')