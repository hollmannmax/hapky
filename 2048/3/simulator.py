import copy
import inspect

def numbers_to_left(in_row):
    score = 0
    result = []
    hold = 0
    for x in in_row:
        if x == 0:
            pass
        elif hold == 0:
            hold = x
        elif hold == x:
            result.append(x + x)
            score += x + x
            hold = 0
        else:
            result.append(hold)
            hold = x
    result.append(hold)
    for x in range(len(in_row) - len(result)):
        result.append(0)


    return result, score


def move_tiles(in_board, direction):
    """Move tiles on board in the specified direction.

    Return a new copy of the board and the total score of the move.
    """
    out_board = []
    out_score = 0
    if direction == 'left':
        for line in in_board:
            board, score = numbers_to_left(line)
            out_board.append(board)
            out_score += score
    elif direction == 'right':
        for line in in_board:
            board, score = numbers_to_left(list(reversed(line)))
            out_board.append(list(reversed(board)))
            out_score += score
    elif direction == 'up':
        in_board = list(map(list, zip(*in_board)))
        for line in in_board:
            board, score = numbers_to_left(line)
            out_board.append(board)
            out_score += score
        out_board = list(map(list, zip(*out_board)))
    elif direction == 'down':
        in_board = list(map(list, zip(*in_board)))
        for line in in_board:
            board, score = numbers_to_left(list(reversed(line)))
            out_board.append(list(reversed(board)))
            out_score += score
        out_board = list(map(list, zip(*out_board)))
    #
    # ... add your code here ...
    #)
    return out_board, out_score
